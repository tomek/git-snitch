* 4 [func] razvan
    The code now uses git --porcelain to parse the ownership details when using
    git-snitch contrib.
    (Gitlab #6)

* 3 [func] tomek
    `contrib` command added. It parses the repo and returns an ordered list of authors,
    with the number of lines contributed.
    (Gitlab #4)

* 2 [func] tomek

   Major update:
   - Support for multiple users. You can now specify one or more users to search
     activity or commits for.
   - Added basic support for gitlab API, using python-gitlab lib.
   - new command: `activity` (list of activities from gitlab for a single period)
   - new command: `activities` (list of activies over multiple periods)
   - `commits` command renamed to `history`
   - new command: `histories` (list of commits for multiple periods)
   - readme rewritten
   (Gitlab #2)

* 1 [func] @tomek

   Initial revision.

For complete code revision history, see
    http://gitlab.isc.org/isc-projects/keama

LEGEND
* [bug]   General bug fix.  This is generally a backward compatible change,
          unless it's deemed to be impossible or very hard to keep
          compatibility to fix the bug.
* [build] Compilation and installation infrastructure change.
* [doc]   Update to documentation. This shouldn't change run time behavior.
* [func]  New feature.  In some cases this may be a backward incompatible
          change, which would require a bump of major version.
* [sec]   Security hole fix. This is no different than a general bug
          fix except that it will be handled as confidential and will cause
          security patch releases.

*: Backward incompatible or operational change.
