#!/usr/bin/env python3

import argparse
import pathlib
import os
import calendar
import gitlab
import re
from subprocess import Popen, PIPE
from dateutil.parser import parse as dateparse
from datetime import date, timedelta, datetime, timezone
from enum import Enum
from typing import List

DEBUG = False
PRINT_DETAILS = False
GL_DEBUG = False

# Let's ignore binary files. git blame returns junk for them.
CONTRIB_EXCLUDED_FILES = [ '.png', '.p12' ]

# When using git-snitch contrib, it can print the stats every specified number of files.
# This is convenient, if you want to get the details while the stats being gathered.
CONTRIB_PRINT_AUTHORS_EVERY_X_FILES = 1000


class Period(Enum):
    """Enumerates supported periods"""
    YEAR = 1
    MONTH = 2
    WEEK = 3


class Format(Enum):
    """Enumerates supported output formats"""
    CSV = 1
    JSON = 2
    TXT = 3


def run_cmd(cmd):
    """executes a command, returns exit code and a stdout

    :param cmd: command to be executed, either as a string or array
    :return: tuple (exit_code, stdout)
    """
    if isinstance(cmd, str):
        cmd = cmd.split(" ")

    with Popen(cmd, stdout=PIPE) as proc:
        output = proc.communicate()[0]
        try:
            return proc.returncode, output.decode("utf-8")
        except UnicodeDecodeError:
            return proc.returncode, output.decode("latin-1")


def sanity_check(args):
    """ checks if passed parameters are valid"""

    if args.command == "histories":
        if args.user == "" and args.email == "":
            raise ValueError("Need to specify at least username or email")
    elif args.command == "activity":
        if args.url == "":
            raise ValueError("Need to specify URL of the GitLab instance")
        if args.private_token == "":
            raise ValueError("Need to specify private token to access the GitLab instance")


def git_to_dirname(git: str):
    """ attempts to convert git remote to a directory name

    git@gitlab.isc.org:tomek/git-activity.git => tomek-git-activity

    :param git: git remote
    :return: dir-safe
    """

    # Returns server name, and path
    _, path = git.split(":")
    return path.replace("/", "-").replace(".git", "")


def checkout_repo(remote, directory):
    """ checks out a single repo to specified directory"""

    # Temporary skipping checkout
    # return

    if not os.path.isdir(directory):
        # Directory doesn't exist, clone a fresh repo
        print(f"--- directory {directory} doesn't exist, cloning new repo {remote}")
        result = run_cmd(f"git clone {remote} {directory}")
        print(f"Clone complete, result {result}")
        return

    print(f"--- dir {directory} exists, updating repo.")
    os.chdir(directory)
    run_cmd("git reset --hard")
    run_cmd("git checkout master")
    result = run_cmd("git pull")
    os.chdir("..")

    print(f"Repo updated: {result}")


def list_repos(repos_list_file: str) -> list:
    print(f"Reading list of repos from {repos_list_file}")
    repos = []
    with open(repos_list_file, 'r', encoding='utf-8') as f:
        for line in f:
            line = line.strip()
            if not len(line) or line[0] == '#':
                continue
            directory = git_to_dirname(line)
            repos.append({"remote": line, "dir": directory})
    return repos


def checkout_repos(args):
    """1. creates directory (if missing)
       2. checks out all directories (if missing) or
          cleans repositories (if present)

       returns a list of repos, each repo is a tuple (dir, git-remote) """

    print(f"Checking out into {args.repodir}")
    pathlib.Path(args.repodir).mkdir(parents=True, exist_ok=True)

    repos = list_repos(args.list)

    os.chdir(args.repodir)

    print(f"Read {len(repos)} repo(s):")
    for line in repos:
        checkout_repo(line["remote"], line["dir"])

    return repos


def process_repos(repos, users: list[str], since, until):

    all_commits = []
    for r in repos:

        commits = process_repo(r, users, since, until)
        all_commits.extend(commits)

    return all_commits


def find_strings(s: str, substrings: list) -> bool:
    """Returns True if any of the substrings is found in s"""
    for sub in substrings:
        if s.find(sub) != -1:
            return True
    return False


# flake8: noqa: F821 - flake8 is too dumb to understand the 'commit' in locals() check.
def parse_git_log(lines: str, repo_name) -> list:
    commits = []

    # Parse the log, get the list of all commits of all users
    for line in lines:
        if line.startswith('commit'):
            if 'commit' in locals():

                # If this is an empty or merge commit, there may be no stats.
                if 'stat' not in commit:
                    commit['stat'] = "0 files changed, 0 insertions(+), 0 deletions(-)"
                if 'insertions' not in commit:
                    commit['insertions'] = 0
                if 'deletions' not in commit:
                    commit['deletions'] = 0
                if 'files' not in commit:
                    commit['files'] = 0
                commits.append(commit)  # noqa: F821
            commit = {"commit": line[7:], "dir": repo_name}
            continue

        if line.startswith('Author: '):
            commit["author"] = line[8:]
            continue

        if line.startswith('Date: '):
            commit["date"] = dateparse(line[6:])
            continue

        # Remaining lines are the log
        if line.startswith('    '):
            if 'log' in commit.keys():
                commit["log"] = commit["log"] + line.strip()
            else:
                commit["log"] = line.strip()

        if "file changed" in line or "files changed" in line:
            # print(f"#### stat found {line}")
            commit['stat'] = line.strip()
            regex = "([0-9]+) files? changed, ([0-9]+ insertions?\\(\\+\\))?(, )?([0-9]+ deletions?)?"
            matches = re.search(regex, line)
            if matches:
                if matches[1] is not None:
                    commit['files'] = int(matches[1])
                else:
                    commit['files'] = 0
                if matches[2] is not None:
                    commit['insertions'] = int(matches[2].split(" ")[0])
                else:
                    commit['insertions'] = 0
                if matches[4] is not None:
                    commit['deletions'] = int(matches[4].split(" ")[0])
                else:
                    commit['deletions'] = 0
            else:
                print(f"ERROR: Unable to parse stat: {line}")
                commit['files'] = 0
                commit['insertions'] = 0
                commit['deletions'] = 0

    if 'commit' in locals():

        # If this is an empty or merge commit, there may be no stats.
        if 'stat' not in commit:
            commit['stat'] = "0 files changed, 0 insertions(+), 0 deletions(-)"
        if 'insertions' not in commit:
            commit['insertions'] = 0
        if 'deletions' not in commit:
            commit['deletions'] = 0
        if 'files' not in commit:
            commit['files'] = 0
        commits.append(commit)  # noqa: F821

    print(f"Found {len(commits)} commits in {repo_name}")

    return commits


def git_log(repo_name):
    """ Returns a list of commits for a given repository. """

    os.chdir(repo_name)

    _, log_all = run_cmd("git log --stat --all")

    lines = log_all.split("\n")

    commits = parse_git_log(lines, repo_name)

    os.chdir("..")

    return commits


def process_repo(repo, users: List[str], since, until):
    """ Processes specified repo and returns a list of commits done by specified users. """

    # Step 1: Get git log, parse it
    commits = git_log(repo["dir"])

    # Step 2: filter out the entries that done by someone else
    filtered_authors = list(filter(lambda x: (find_strings(x['author'], users)), commits))

    # Step 3: filter out the entries that are not in the specified time range
    filtered_dates = list(filter(lambda x: (x['date'] >= since and x['date'] <= until), filtered_authors))

    # Debugging of the filtering
    # for commit in commits:
    #     if find_strings(commit['author'], users):
    #         print(f"### YES: {commit['commit']} by {commit['author']} in {repo['dir']}")
    #     else:
    #         print(f"### NO:  {commit['commit']} by {commit['author']} in {repo['dir']}")
    print(f"Git log for {repo['dir']} has {len(commits)} commits, {len(filtered_authors)} filtered by users, "
          f"{len(filtered_dates)} filtered by date.")

    return filtered_dates


def print_git_stats(commits, format: Format, since=None, until=None):
    """Prints git stats in a given format"""

    cnt = 0
    insertions = 0
    deletions = 0
    files = 0

    for commit in commits:
        cnt = cnt + 1
        insertions = insertions + commit['insertions']
        deletions = deletions + commit['deletions']
        files = files + commit['files']

    if format == Format.CSV:
        print(f"{since.strftime('%y-%m-%d')},{until.strftime('%y-%m-%d')},{cnt},{files},{insertions},{deletions}")
    elif format == Format.JSON:
        print(f"\\{ 'since': '{since}', 'until':'{until}', 'commits':'{cnt}','files':{files}, 'insertions':{insertions},'deletions': {deletions} \\}")
    elif format == Format.TXT:
        print(f"{cnt} commits with lines added {insertions}/ deleted {deletions}, {files} touched between {since} and {until}")

    # if format == Format.CSV:
    #     print("commit,author,date,dir,log")
    #     for commit in commits:
    #         print(f"{commit['commit']},{commit['author']},{commit['date']},{commit['dir']},{commit['log']}")
    # elif format == Format.JSON:
    #     print(commits)
    # elif format == Format.TXT:
    #     for commit in commits:
    #         print(f"{commit['commit']} by {commit['author']} in {commit['dir']} on {commit['date']} ({commit['files']} files, {commit['insertions']}+{commit['deletions']}-): {commit['log']}")


def print_repo_stats(commits, since, until, period: Period):

    commits_sorted = sorted(commits, key=lambda x: x["date"])

    by_month = [0] * 13
    cnt = 0

    print(f"--- list of commits {len(commits_sorted)}---")
    for line in commits_sorted:
        if line["date"] >= since and line["date"] <= until:
            commit = line["commit"][0:8]
            if PRINT_DETAILS:
                print(f'{commit}@{line["dir"]} on {line["date"].strftime("%Y-%m-%d")} by {line["author"]}: {line["log"][:80]}')

            by_month[line["date"].month] = by_month[line["date"].month] + 1

            cnt = cnt + 1

    if period == Period.YEAR:
        print("--- count by month ---")
        for m in range(1, 13):
            print(f"Commits in month {m}: {by_month[m]}")
    print(f'--- {cnt} commits between {since.strftime("%Y-%m-%d")} and {until.strftime("%Y-%m-%d")}')


def action_to_key(action: str) -> str:
    """Converts action to a key used in summary dictionary"""
    return action.replace(" ", "_").lower()


def gitlab_activity_year(args, year: int, usernames: list = None):
    print(f"INFO: Connecting to instance at {args.url}, secret token is {args.private_token[:6]}... ({len(args.private_token)} bytes)")
    gl = gitlab.Gitlab(url=args.url, private_token=args.private_token)

    if GL_DEBUG:
        print("DEBUG: gitlab debugging info enabled.")
        gl.enable_debug()

    # Get the list of activities for each month
    oneday = timedelta(days=1)
    # TODO: Fix this hardcoded range
    for m in range(6, 10):
        first = date(year, m, 1)
        last = date(year, m, calendar.monthrange(year, m)[1])

        after = (first - oneday).strftime("%Y-%m-%d")
        before = (last + oneday).strftime("%Y-%m-%d")

        summary = gitlab_activity_timespan(gl, usernames, after, before)
        total = 0
        for k in summary.keys():
            total = total + summary.get(k, 0)
        summary['total'] = total
        summary["first"] = first.strftime("%Y-%m-%d")
        summary["last"] = last.strftime("%Y-%m-%d")
        summary["period"] = f"{year}-{m:02d}"

        print_activity_summary(summary, first, last, usernames, args.format)


def gitlab_activity_timespan(gl, usernames: list, after: str, before: str):
    """Gets a gitlab activity for a list of users between two dates"""

    users = []

    summary = {}

    for u in usernames:
        id, activity = gitlab_user_activity(gl, u, after, before)
        if id:
            users.append({'user': u, 'id': id, 'activity': activity})

    for u in users:
        if DEBUG:
            print(f"// User {u['user']}(id={u['id']}) has {u['activity'].total} activities after {after} and before {before}")
        for e in u['activity']:
            action = action_to_key(e.action_name)

            if PRINT_DETAILS:
                print(f"Activity {e.action_name} in project {e.project_id} at {e.created_at}, adding to {action}, details: {e}")
            if action in summary.keys():
                summary[action] = summary.get(action, 0) + 1
            else:
                summary[action] = 1
        if DEBUG:
            print(f"DEBUG: Summary after processing user {u['user']}: {summary}")

    if DEBUG:
        print(f"DEBUG: Summary: {summary}")

    return summary


def q(dictionary, key):
    """Returns a value from a dictionary, or 0 if the key is not present"""
    return dictionary.get(key, 0)


def print_activity_summary(summary, after, before, users, format: Format):
    users_str = " ".join(users)
    if format == Format.CSV:
        if DEBUG:
            print(f"Activities between {after} and {before} by users {users_str}:")
        # for k in sorted(summary.keys()):
        #    print(f"{k},{summary[k]},", end='')
        # print()
        print("from, to, who, total, accepted,approved,closed,commented_on,deleted,destroyed,opened,pushed_new,pushed_to,updated,")
        print(f"{after},{before},{users_str}, {summary['total']},{q(summary, 'accepted')},{q(summary, 'approved')},"
              f"{q(summary, 'closed')},{q(summary, 'commented_on')},{q(summary, 'deleted')},{q(summary, 'destroyed')},"
              f"{q(summary, 'opened')},{q(summary, 'pushed_new')},{q(summary, 'pushed_to')},{q(summary, 'updated')},")
    elif format == Format.JSON:
        print(f"// Activities between {after} and {before} by users [{users_str}]:")
        print(f"{summary},")
    elif format == Format.TXT:
        print(f"Last week, the team ({users_str}) pushed {q(summary, 'pushed_to')} new commits, created {q(summary, 'pushed_new')} new branches,")
        print(f"opened {q(summary, 'opened')} new and closed {q(summary, 'closed')} issues, posted {q(summary,'commented_on')} comments.")
        print(f"{q(summary, 'created')} new items (such as wiki pages or gists) were created and {q(summary,'updated')} were updated.")
        print(f"{q(summary, 'accepted')} branches were merged.")


def gitlab_user_activity(gl, username: int, after: str, before: str) -> int:
    """ This function returns a list of activities for a given user between two dates"""

    # First, let's get the user's id
    try:
        user = gl.users.list(username=username)[0]
    except IndexError:
        print(f"User {username} not found")
        return None, None

    events = gl.users.get(user.id).events.list(query_parameters={'after': after, 'before': before, 'per_page': 10, id: user.id}, iterator=True)

    return user.id, events

    # for u in users:
    #     user = gl.users.list(username=u)[0]
    #     print(f"User {u} has id {user.id}")
    #     activities = gl.events.list(
    #         query_parameters={'id': 'wlodek', 'after': '2023-07-30', 'before': '2023-09-01', 'per_page': 10},
    #         get_all=False,
    #     )
    #     for activity in activities:
    #         print(f"{activity}\n")
    #     print(f"--- Found {len(activities)} activities ---")

    # https://python-gitlab.readthedocs.io/en/stable/gl_objects/users.html#id17


def list_projects(gl):
    projects = gl.projects.list(iterator=True)
    for project in projects:
        print(f"id:{project.id} name:{project.name} path:{project.path_with_namespace} url:{project.web_url}")
        break
    print(f"Found {len(projects)} projects")


def period_to_enum(period: str) -> Period:
    """Converts a string to a Period enum"""
    if period == "year":
        return Period.YEAR
    elif period == "month":
        return Period.MONTH
    elif period == "week":
        return Period.WEEK

    raise ValueError(f"Invalid period: {period}")


def format_to_enum(format: str) -> Format:
    if format.lower() == "csv":
        return Format.CSV
    elif format.lower() == "json":
        return Format.JSON
    elif format.lower() == "txt":
        return Format.TXT

    raise ValueError(f"Invalid format: {format}")


def get_period_start_end(period: Period, day: date = None) -> (date, date):
    """Returns start and end date for a given period. For week, it returns
       last week"""

    if day is None:
        day = date.today()

    if period == Period.YEAR:
        # return the current year
        since = day.replace(month=1, day=1)
        until = day.replace(month=12, day=31)
    elif period == Period.MONTH:
        # return the current month
        since = day.replace(day=1)
        until = day.replace(day=calendar.monthrange(day.year, day.month)[1])
    elif period == Period.WEEK:
        # return last week
        since = day - timedelta(days=7 + day.weekday())
        until = day - timedelta(days=day.weekday() + 1)
    else:
        raise ValueError(f"Invalid period: {period}")

    return datetime(since.year, since.month, since.day, 0, 0, 0, tzinfo=timezone.utc), \
        datetime(until.year, until.month, until.day, 23, 59, 59, tzinfo=timezone.utc)


def gitlab_activity(gl, args, period: Period, first: date, last: date, usernames: list):
    """Gets a gitlab activity for a list of users for specified year in month intervals"""

    # Get the list of activities for each month
    oneday = timedelta(days=1)
    after = (first - oneday).strftime("%Y-%m-%d")
    before = (last + oneday).strftime("%Y-%m-%d")

    summary = gitlab_activity_timespan(gl, usernames, after, before)
    total = 0
    for k in summary.keys():
        total = total + summary.get(k, 0)
    summary['total'] = total
    summary["first"] = first.strftime("%Y-%m-%d")
    summary["last"] = last.strftime("%Y-%m-%d")

    print_activity_summary(summary, first, last, usernames, args.format)

    return summary


def parse_users(users: str) -> list:
    """Parses a list of users from a string"""
    return users.split(",")


def period_to_days(period: Period) -> int:
    if period == Period.YEAR:
        return 365
    elif period == Period.MONTH:
        return 31
    elif period == Period.WEEK:
        return 7
    else:
        raise ValueError(f"Invalid period: {period}")


def get_periods(period: Period, since: date, until: date) -> list:
    """Returns a list of periods between two dates"""

    periods = []
    old_since = since

    if DEBUG:
        print(f"DEBUG: Getting {period} periods between {since} and {until}")
    while since <= until:
        start, end = get_period_start_end(period, since)
        if period == Period.WEEK:
            # Week is peculiar in the sense that it returns the previous week, not this week
            # we need to adjust for that.
            start = start + timedelta(days=period_to_days(period))
            end = end + timedelta(days=period_to_days(period))
        if DEBUG:
            print(f"DEBUG: Period {start} - {end}")
        periods.append((start, end))
        since = end + timedelta(days=1)
        if DEBUG:
            print(f"DEBUG: Next period starts on {since}")

    if DEBUG:
        print(f"{len(periods)} {period} periods between {old_since} and {until}")

    return periods


def get_since_until(args):
    if args.since != "" and args.until != "":
        return dateparse(args.since).replace(tzinfo=timezone.utc), dateparse(args.until).replace(tzinfo=timezone.utc)

    if args.period == Period.YEAR:
        return date.today().replace(year=date.today().year - 1, month=1, day=1), date.today()
    elif args.period == Period.MONTH:
        return date.today().replace(month=date.today().month - 1, day=1), date.today()
    elif args.period == Period.WEEK:
        return get_period_start_end(Period.WEEK, date.today())

def print_authors(authors, print_files=False):
    ordered_authors = sorted(authors.items(), key=lambda x: x[1]['lines'], reverse=True)

    for key, value in ordered_authors:
        print(f"Author [{value['name']}] email [{key}] has {value['lines']} lines")
        if print_files:
            for file, lines in value['files'].items():
                print(f'\t- {lines}\t{file}')

def append_authors(authors, new_authors, file):
    for author in new_authors:
        #print(f"DEBUG:AA  Adding {author}")
        if author not in authors:
            authors[author] = {
                'name': new_authors[author]['name'],
                'files': {},
                'lines': 0
            }
        authors[author]['files'][file] = new_authors[author]['count']
        authors[author]['lines'] = authors[author]['lines'] + new_authors[author]['count']

def file_excluded(file):
    # find extension
    ext = os.path.splitext(file)[1]
    return ext in CONTRIB_EXCLUDED_FILES

def blame_parse(blame_all):
    """Parses git blame output and returns a list of authors with number of lines for each author"""
    authors = {}

    line_cnt = 0
    for line in blame_all.splitlines():
        if line.find('author ') != -1:
            name = line.split('author ')[1]
        if line.find('author-mail ') != -1:
            author = line.split('author-mail ')[1]
            if author in authors:
                authors[author]['count'] = authors[author]['count'] + 1
            else:
                authors[author] = {}
                authors[author]['count'] = 1
                authors[author]['name'] = name
            line_cnt = line_cnt + 1

    return authors, line_cnt

def git_blame_repo(remote, directory, print_files=False):
    """Gets a list of authors for one specific repository"""
    os.chdir(directory)

    print(f"DEBUG: Im in {os.getcwd()} directory")

    print(f"--- dir {directory} exists, getting blame info.")

    # First, get a list of files.
    _, files = run_cmd("git ls-files")

    files = files.strip().split("\n")

    print(f"INFO: found {len(files)} files in {remote}")

    global_authors = {}
    total_lines = 0

    cnt = 0
    for file in files:
        cnt = cnt + 1

        # check if this is not a binary file
        if file_excluded(file):
            print(f"INFO: Skipping excluded file {file}")
            continue

        cmd = f"git blame --porcelain --line-porcelain --date=iso {file}"
        if DEBUG:
            print(f"DEBUG: Running command {cmd}")
        _, blame_all = run_cmd(cmd)

        authors, lines = blame_parse(blame_all)

        total_lines += lines

        append_authors(global_authors, authors, file)
        print(f"File {file} parsed: found {lines} lines ({total_lines} lines parsed so far, {len(global_authors)} author(s) found so far.")
        # print_authors(authors)

        if (cnt % CONTRIB_PRINT_AUTHORS_EVERY_X_FILES) == 0:
            print_authors(global_authors)

    os.chdir("..")
    print("SUMMARY")
    print_authors(global_authors, print_files)

    return global_authors


def git_blame_all_repos(args):
    """Generates a list of authors with number of lines for each author"""
    repos = list_repos(args.list)

    os.chdir(args.repodir)

    for r in repos:
        git_blame_repo(r['remote'], r['dir'], args.print_files)

    os.chdir("..")

def main():
    """The main function of the tool. Parses options, then runs the tool."""
    parser = argparse.ArgumentParser('git-snitch')
    parser.add_argument("-d", "--debug", action='store_true', help="enable debugging")
    parser.add_argument("-u", "--user", type=str, default='Mrugalski', help="name of the committer to search for (or coma separated list)")
    parser.add_argument("--repodir", type=str, default='repos', help="dir where to store checked out repos")
    parser.add_argument("-l", "--list", type=str, default='repos.txt', help="filename that contains a list of repos (1 repo per line)")
    parser.add_argument("--since", type=str, default="", help="after that date")
    parser.add_argument("--until", type=str, default="", help="till that date")
    parser.add_argument("-p", "--period", type=str, default="year", help="Period (year, month, or week) to analyze")
    parser.add_argument("-f", "--format", type=str, default="csv", help="Output format (csv or json)")
    parser.add_argument("--dry-run", action='store_true', default=False, help="Dry run (don't actually checkout repos)")
    parser.add_argument("-t", "--private-token", type=str, default='', help="Private token to access the GitLab instance")
    parser.add_argument("--details", action='store_true', help="Print details of each activity or commit (warning: large output!)")
    parser.add_argument("--lines", action='store_true', help="Print number of lines added/removed in each commit")

    subparsers = parser.add_subparsers(help='commands', dest="command")
    subparsers.add_parser("history", help='Get commits for one single period')
    subparsers.add_parser('histories', help='Get commits for many periods')
    subparsers.add_parser('update', help='Checks out or update GIT repositories')
    activity_parser = subparsers.add_parser('activity', help='Get a list of activities from GitLab for a single period')
    activity_parser.add_argument("--url", type=str, default='https://gitlab.isc.org', help="URL of the GitLab instance")
    activities_parser = subparsers.add_parser('activities', help='Get a list of activities from GitLab for multiple periods')
    activities_parser.add_argument("--url", type=str, default='https://gitlab.isc.org', help="URL of the GitLab instance")
    contrib_parser = subparsers.add_parser('contrib', help='Get a list of contributors with a number of lines')
    contrib_parser.add_argument("--print-files", action='store_true', help='print the number of lines for each file')

    args = parser.parse_args()
    args.format = format_to_enum(args.format)
    args.period = period_to_enum(args.period)

    sanity_check(args)

    global DEBUG, GL_DEBUG, PRINT_DETAILS

    if args.debug:
        print("DEBUG: Debugging enabled.")
        DEBUG = True

    if args.details:
        print("INFO: Printing details of each activity or commit enabled.")
        PRINT_DETAILS = True

    since, until = get_since_until(args)

    #    since, until = get_period_start_end(args.period, since)
    #    if since == dateparse("2023-01-01"):
    #        since = date.today()

    if args.command is not None:
        print(f"INFO: Running git-snitch for {args.period} period(s) between {since} and {until}")

    if args.command in ['activity', 'activities']:

        users = parse_users(args.user)

        if users is None:
            users = ['andrei', 'fdupont', 'marcin', 'mgodzina', 'piotrek', 'razvan', 'slawek', 'wlodek', 'tmark', 'tomek']

        print(f"INFO: Connecting to instance at {args.url}, secret token is {args.private_token[:6]}... ({len(args.private_token)} bytes)")
        gl = gitlab.Gitlab(url=args.url, private_token=args.private_token)

        if GL_DEBUG:
            print("DEBUG: gitlab debugging info enabled.")
            gl.enable_debug()

    if args.command == "history":
        repos = list_repos(args.list)

        print(f"INFO: Getting commits between {since} and {until}")

        if args.dry_run:
            print(f"INFO: Dry run, would retrieve data from {since} to {until}.")
        else:
            users = args.user.split(",")
            print(f"INFO: Searching for {len(users)} users: {','.join(users)}.")
            commits = process_repos(repos, users, since, until)

            # print_repo_monthly_stats(commits, since, until)
    elif args.command == 'histories':
        repos = list_repos(args.list)

        users = args.user.split(",")

        os.chdir("repos")

        commits = process_repos(repos, users, since, until)

        periods = get_periods(args.period, since, until)

        if args.format == Format.CSV:
            print("since,until,commits,files changed,lines added, lines deleted")

        for p in periods:
            since, until = p

            # Get only the commits in the specified period
            filtered = list(filter(lambda x: (x['date'] >= since and x['date'] <= until), commits))

            # print(f"INFO: {len(filtered)} commits between {since} and {until}")

            print_git_stats(filtered, args.format, since, until)

    elif args.command == 'update':
        repos = checkout_repos(args)
        print(f"INFO: Updating repos {repos}")

    elif args.command == 'activity':
        print(f"INFO: Retrieving activity for users {users} between {since} and {until}.")
        if args.dry_run:
            print(f"INFO: Dry run, would retrieve data from {since} to {until}.")
        else:
            gitlab_activity(gl, args, args.period, since, until, users)

    elif args.command == 'activities':

        periods = get_periods(args.period, since, until)

        for p in periods:
            since, until = p
            if args.dry_run:
                print(f"INFO: Dry run, would retrieve data from {since} to {until}.")
            else:
                gitlab_activity(gl, args, args.period, since, until, users)
    elif args.command == 'contrib':
        git_blame_all_repos(args)
    else:
        # Should not happen this is already checked by argparse
        print(f"Invalid command: {args.command}")
        parser.print_help()


if __name__ == "__main__":
    main()
