# Git snitch Developer's Guide

Not much here. The tool was written by Tomek mostly for his own needs, but over time, couple use cases
surfaced that might possibly make it useful for others. As initially internal-only, the tool was not
really well thought of.

In fact, the internal looks almost like two tools glued together. The first part is related to
processing of local repos. The second part is related to usage of GitLab API. Since the data formats
and data types are radically different, there is not that much of a common code between them.
There is also some duplication.

The git logs parsing code is a mess, but I haven't found a cleaner way to do it. I'm sure there is one
(git's pretty-printing looks powerful), but I never needed to make it more robust. Razvan improved
the situation in #6 and it's a bit better now.

## CI

There's basic CI that uses `flake8`, `pylint` for linting the code and `pytest` to run tests.
What tests? you might ask. And that would be a very good question.

To run tests, use the following:

`python -m pytest -s -v --ignore=repos/`

To run pylint, use the following:

`python3 -m pylint --rcfile .pylint $(git ls-files '*.py')`

To run flake8, use the following:

`python3 -m flake8 --config .flake8 $(git ls-files '*.py')`

The CI itself is using GitLab's CI pipelines. See `.gitlab-ci.yml` for details.
