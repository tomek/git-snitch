# Notes on using gitlab API

Git-Snitch is using python-gitlab library.


User Activities (git clone/push, user visiting pages, dashboards, etc., logging, using API, )
https://docs.gitlab.com/16.1/ee/api/users.html#get-user-activities




user contribution events:

https://docs.gitlab.com/ee/api/events.html#get-user-contribution-events

https://gitlab.example.com/api/v4/users/:id/events




curl (works)
https://gitlab.isc.org/api/v4/users/153/events?after=2023-07-30&before=2023-09-01



# References

1. Python-Gitlab library: https://python-gitlab.readthedocs.io/
2. User's activity example: https://python-gitlab.readthedocs.io/en/stable/gl_objects/users.html#id17
3. Gitlab REST API v4 - https://docs.gitlab.com/ee/api/rest/index.html


Get user events:
https://docs.gitlab.com/ee/api/events.html#get-user-contribution-events