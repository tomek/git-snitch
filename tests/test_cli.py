from subprocess import Popen, PIPE
from datetime import date, datetime, timezone
from typing import List
import pytest

GIT_SNITCH_PATH = "./git-snitch.py"

snitch = __import__("git-snitch")


def run_cmd(binfile, params, env=None):
    """Runs file specified as binfile, passes all parameters.
    Returns a tuple of returncode, stdout, stderr"""
    par = [binfile]
    if params and len(params):
        par += params

    # @todo: set passed env variable, if any
    p = Popen(par, stdout=PIPE, stderr=PIPE)
    stdout, stderr = p.communicate()
    return p.returncode, stdout.decode("utf-8"), stderr.decode("utf-8")


def check_output(output: str, strings: List[str]):
    """Checks if specified output (presumably stdout) has appropriate content. strings
    is a list of strings that are expected to be present. They're expected
    to appear in the specified order, but there may be other things
    printed in between them. Will assert if string is not found. Returns number
    of found strings."""
    offset = 0
    cnt = 0
    for s in strings:
        new_offset = output.find(s, offset)

        if new_offset == -1:
            assert False, f"Not found an expected string: '{s}' in '{output}'"

        offset = new_offset + len(s)
        cnt += 1
    return cnt


def check_file(file, strings):
    """Checks if specified file contains specified strings."""
    with open(file, "r") as f:
        content = f.read()

    return check_output(content, strings)


def check_command(bin, params, exp_code=0, exp_stdout=[], exp_stderr=[]):
    """Runs specified command (bin) with parameters (params) and expected
    the return code to be exp_code. Returns the (exit code, stdout, stderr) tuple."""

    # Run the command
    result = run_cmd(bin, params)
    print(f"\nRunning command: {bin} {' '.join(params)}, exit code: {result[0]}, " +
          f"stdout size: {len(result[1])}, stderr size: {len(result[2])}")

    # TODO: Interpret exp_code 0 as non-failure (i.e. different than 255)
    if exp_code == 0:
        assert result[0] != 255
    else:
        assert result[0] == exp_code

    if exp_stdout:
        print(f"Checking if stdout (size {len(result[1])}) equals expectations.")
        check_output(result[1], exp_stdout)
    if exp_stderr:
        print(f"Checking if stderr (size {len(result[2])}) equals expectations.")
        check_output(result[2], exp_stderr)

    return result


def test_help_file():
    """ Checks if keama has a help file and it prints parameters."""
    exp = ["usage: git-snitch [-h] [-d]",
           "{history,histories,update,activity,activities,contrib}",
           "positional arguments:",
           "{history,histories,update,activity,activities,contrib}",
           "history             Get commits for one single period",
           "histories           Get commits for many periods",
           "update              Checks out or update GIT repositories",
           "activity            Get a list of activities from GitLab for a single",
           "activities          Get a list of activities from GitLab for multiple",
           "contrib             Get a list of contributors with a number of lines",
           "options:",
           "-h, --help",
           "-d, --debug",
           "-u USER, --user USER",
           "--repodir REPODIR",
           "-l LIST, --list LIST",
           "--since SINCE",
           "--until UNTIL",
           "-p PERIOD, --period PERIOD",
           "-f FORMAT, --format FORMAT"]
    check_command('python', [GIT_SNITCH_PATH], 0, exp, [])


@pytest.mark.skip(reason="Not implemented yet")
def test_version():
    """ Checks if git-snitch can report its version."""

    # TODO The version reported should be set automatically.
    exp = ["git-snitch 0.1.0-git"]
    check_command('python', [GIT_SNITCH_PATH, "-v"], 0, exp, [])
    check_command('python', [GIT_SNITCH_PATH, "--version"], 0, exp, [])


def test_periods():
    assert snitch.period_to_enum("year") == snitch.Period.YEAR
    assert snitch.period_to_enum("month") == snitch.Period.MONTH
    assert snitch.period_to_enum("week") == snitch.Period.WEEK
    with pytest.raises(ValueError):
        snitch.period_to_enum("day")


def date_begin(year, month, day):
    return datetime(year, month, day, 0, 0, 0, tzinfo=timezone.utc)


def date_end(year, month, day):
    return datetime(year, month, day, 23, 59, 59, tzinfo=timezone.utc)


def test_get_period_start_end():
    assert snitch.get_period_start_end(snitch.Period.YEAR, date(2020, 1, 1)) == (date_begin(2020, 1, 1), date_end(2020, 12, 31))
    assert snitch.get_period_start_end(snitch.Period.MONTH, date(2023, 9, 14)) == (date_begin(2023, 9, 1), date_end(2023, 9, 30))
    assert snitch.get_period_start_end(snitch.Period.WEEK, date(2023, 9, 14)) == (date_begin(2023, 9, 4), date_end(2023, 9, 10))


def test_get_periods():
    """ This function splits given period into smaller ones."""
    cases = [
        # When since > until, it should return empty list
        {"period": snitch.Period.WEEK, "since": date(2023, 7, 1), "until": date(2023, 6, 30), "expected": []},

        # Normal, non degenerate case. When asked for a month, it should return all weeks in that month, including
        # those that partially overlap with other months.
        {"period": snitch.Period.WEEK, "since": date_begin(2023, 7, 1), "until": date_end(2023, 7, 31), "expected": [
            (date_begin(2023, 6, 26), date_end(2023, 7, 2)),
            (date_begin(2023, 7, 3), date_end(2023, 7, 9)),
            (date_begin(2023, 7, 10), date_end(2023, 7, 16)),
            (date_begin(2023, 7, 17), date_end(2023, 7, 23)),
            (date_begin(2023, 7, 24), date_end(2023, 7, 30)),
            (date_begin(2023, 7, 31), date_end(2023, 8, 6))
        ]
        },

        # Special case - when since and until are the same day, it should return a single period
        {
            "period": snitch.Period.WEEK,
            "since": date_begin(2023, 9, 18),
            "until": date_end(2023, 9, 18),
            "expected": [(date_begin(2023, 9, 18), date_end(2023, 9, 24))]
        },
        {
            "period": snitch.Period.MONTH,
            "since": date_begin(2023, 9, 18),
            "until": date_begin(2023, 9, 18),
            "expected": [
                (date_begin(2023, 9, 1), date_end(2023, 9, 30))
            ]
        },
    ]

    for case in cases:
        assert snitch.get_periods(case["period"], case["since"], case["until"]) == case["expected"]

# TODO: implement parsing of the tests history. The test might use part of git-snitch history itself.
