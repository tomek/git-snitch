from subprocess import Popen, PIPE
from shutil import rmtree
import os

snitch = __import__("git-snitch")


def run_cmd(binfile, params, env=None):
    """Runs file specified as binfile, passes all parameters.
    Returns a tuple of returncode, stdout, stderr"""
    par = [binfile]
    if params and len(params):
        par += params

    # @todo: set passed env variable, if any
    p = Popen(par, stdout=PIPE, stderr=PIPE)
    stdout, stderr = p.communicate()
    return p.returncode, stdout.decode("utf-8"), stderr.decode("utf-8")


def test_contrib():
    """ Checks if `contrib` command works properly. It clones a small repo, then generates a report
        and compares it with the expected values."""

    # first, we need to clone a repo. Let's pick something small.
    rmtree("kea-docker", ignore_errors=True)

    status_code, _, _ = run_cmd("git", ["clone", "https://gitlab.isc.org/isc-projects/kea-docker.git"])
    assert status_code == 0

    os.chdir('kea-docker')

    status_code, _, _ = run_cmd("git", ["reset", "--hard", "afa3c7c59ee0d26f47e9061a7fbee8925d60fd9d"])
    assert status_code == 0

    os.chdir("..")

    authors = snitch.git_blame_repo("https://gitlab.isc.org/isc-projects/kea-docker.git", "kea-docker")

    assert authors is not None

    authors = {v['name']: v['lines'] for k, v in authors.items()}

    # This is the expected list of authors and their contributions for kea-docker
    exp = {'Andrei Pavel': 21, 'Wlodek Wencel': 1694, 'Marcin Godzina': 377, 'mgodzina': 201, 'Tomek Mrugalski': 143}

    assert sorted(authors) == sorted(exp)
