# Git Snitch

<img align="right" border="2em" src="doc/snitch-200px.png">

The overall goal of this tool is to provide a summary of activity of a team of developers across multiple projects. It
can be used to report the activity of multiple people across multiple GIT repositories. If the projects are kept in
GitLab, it can additionally use GitLab API to retrieve and provide additional statistics, such as number of tickets
opened, comments, merge requests closed, etc.

It has several modes of operations:

- **update**: this is the basic step needed for `history` and `histories` commands. Make sure you edit the repos.txt
  file with a list of repos you'd like to get updated. If local copies don't exist, snitch will clone them for you.

- **history**: it walks through multiple git repos and generates a list of all commits done by a specified user or a
list of users. You can optionally specify the time constraints for a single period, e.g. last year or last week.

- **histories**: similar to `history` command, except you can do this for multiple periods, e.g. you can get statistics
for each month of the year, each week or any other period you find useful.

- **activity**: uses gitlab API to generate statistics of specific user or list of users activity. This can be used to
track team's activity as a whole. Time constraints can be specified.

- **activities**: similar to `activity` command, but for multiple periods. Similar to `histories`, this can be used to
  statistics for each month of the year, each week etc.

- **contrib**: generates a list of authors, with a number of lines they committed that's still on master.

## Documentation

- [README](https://gitlab.isc.org/tomek/git-snitch/-/blob/master/README.md) - you are reading it now
- [ChangeLog](ChangeLog.md) - basic history of changes
- [GitLab API notes](doc/gitlab-api.md) - Tomek's notes written while exploring the GitLab API
- [Developer's guide](doc/devel.md) - very brief notes about the tool's internals.

## Installation

This is pretty standard Python stuff:

```
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

## usage overview

Snitch has basic built-in help. See `python git-snitch.py --help` for details. You can get help specific
for a command, e.g. `python git-snitch.py activity -h`, but there are not many command specific options.

Snitch provides several commands. Some of them (`history`, `histories` and probably others in the future)
require a local copy of the repositories. Before using it for the first time, you should edit `repos.txt` file.
Alternate file can be specified with `-l` or `--list`. The repositories will be cloned into `repos/` in the
current directory, unless you specify some other location with `--repodir`.

For many commands, you can specify the period with `-p` (week, month, year). The tool attempts to guess the
time boundaries, e.g. for a week, it will attempt to report on the last week. You can explicitly specify the
boundaries with `--since` and `--until`.

If you want to retrieve GitLab statistics (`activity`, `activities` commands), a GitLab access token is necessary. You
can create one by going to your GitLab instance, click on your profile photo, preferences, then access tokens and create
one that has `read_api`, `read_user`, `read_repository`, `read_registry` capabilities enabled. (For the time being,
probably only read_api is really used).

The tool can provide results in several formats (see `-f`): json, csv, and plain text. Some formats are better maintained than
others. In most cases, some manual edits are necessary. Sorry, this tools is not very mature.

An example usage:

```
python3 ./git-snitch.py -t <secret-token> --format txt -u andrei,marcin,tomek -p week activity
```

### Usage: update

Clones or updates your local copyies of all the repos. Please make sure the `repos.txt` file lists all the repos. One
git repo per line. Comments (`#`) and empty lines ignored. Depending on the number and size of your repos, this might take
a while.

It will create `repos` dir and will check out all specified repos there. If repos already exist, they will be forcibly
updated (if you're careless enough to have uncommited changes in a repo and you allow a random script from the Internet
to govern it, it's your fault).

### Usage: history

You may probably want to run `update` command to make sure the repos are up to date. Run the script `python3
git-snitch.py commits`. The script has some basic knobs. Run `python3 git-snitch.py --help` for details.
An example usage looks like this:

`python ./git-snitch.py -u tomek --since 2023-07-01 --until 2023-09-01 history`

### Usage: histories

Parses git history, splits specified time range into weeks (or other specified periods) and for each week generates statistics.

Example usage:
`python ./git-snitch.py --format csv -u alice,bob,charlie --since 2023-05-01 --until 2023-09-25 -p week histories`

### Usage: activity

This command will show the activity for one single defined period. For a week, it's the last week that just
concluded. For a month, it's the current month. For a year, it's the current year.

Example usage that will get you the activity of the team:
`python ./git-snitch.py -f csv -p week activity -u alice,bob,charlie -t <token>`

It's a bit confusing what the GitLab API actually reports. My interpretation (I might be wrong!) is that
`commented_on` even means someone posted a comment on a ticket or MR, `pushed_to` means a commit was pushed to existing
branch, `pushed_new` means someone pushed a new branch, `approved` means some reviewer clicked on the Approve button,
`created` applies to wiki pages and possibly snippets, `opened` and `closed` applies to issues, `deleted` applies to
branches or issues (gitlab admins can delete issues).

### Usage: activities

Activities command is similar to activity, but it can span multiple periods, e.g. the following command will
show activities for each week between July 1st and Sep 1st. This also includes weeks that partially overlap
preceding and following months.

`python ./git-snitch.py -f json -u tomek,wlodek --since 2023-07-01 --until 2023-09-01 -p week activities -t <token>`


## Usage: contrib

Generates a list of authors, sorted by the number of lines they contributed. Usage is simple:

`python ./git-snitch.py contrib`

Optionally, you can use `--print-files` to generate a list of files for each author. Use with caution on larger repos,
as it can generate a lot of content.
